#!/usr/bin/python


import services
from objects import *
import unittest
import requests
import urllib3
from selenium import webdriver
from nose.tools import assert_true
import os
from main import app
from main import mongo

class readAddDev(unittest.TestCase):
    def setUp(self):
        self.headers={'Accept':'application/json',
                'Autorization':'Bearer b2f3572899ae20c5ccb41f4fbac7e89b6cb08205',}
        self.URL= "https://172.16.76.134/api/device/mac/"
        urllib3.disable_warnings()
    def test_read_formTC1(self):
        print "Executing Testcase - API read_add_device_form()"
        formInfo = {'address':'00-00-00-00-00-00','IP':'172.16.14.34','owner':'Hello','building':'ABW','type':'123','hostname':'sample','nickname':'tom'}
        userDict={'username':'tom','pvid':'1','name':'tommy','email':'tom@dl.com'}
        user = User(userDict['username'],userDict['pvid'],userDict['name'],userDict['email'])
        result = services.read_add_device_form(formInfo,user,adminFlag=False)
        self.assertEqual(result.IP,'None')
    def test_read_formTC2(self):
        print "Executing Testcase - API read_add_device_form()"
        formInfo = {'address':'00-00-00-00-00-00','IP':'172.16.14.34','owner':'Hello','building':'ABW','type':'123','hostname':'sample','nickname':'tom'}
        userDict={'username':'tom','pvid':'1','name':'tommy','email':'tom@dl.com'}
        user = User(userDict['username'],userDict['pvid'],userDict['name'],userDict['email'])
        result = services.read_add_device_form(formInfo,user,adminFlag=True)
        self.assertEqual(result.owner,'Hello')
    def test_read_formTC3(self):
        print "Executing Testcase - API read_add_device_form()"
        formInfo = {'address':'00-00-00-00-00-00','IP':'172.16.14.34','owner':None,'building':'ABW','type':'123','hostname':'sample','nickname':'tom'}
        userDict={'username':'tom','pvid':'1','name':'tommy','email':'tom@dl.com'}
        user = User(userDict['username'],userDict['pvid'],userDict['name'],userDict['email'])
        result = services.read_add_device_form(formInfo,user,adminFlag=True)
        self.assertEqual(result.owner,'tom')

    def tearDown(self):
        pass


class insSyncDev(unittest.TestCase):
    def test_insDevTC1(self):
        print "Executing Testcase - API insert_and_sync_devices()"
        devDict = {'address':'00-00-00-00-00-FF','IP':'172.16.14.34','owner':'Hello','creator':'sam','building':'ABW','type':'123','hostname':'sample','nickname':'tom'}
        userDict={'username':'tom','pvid':'1','name':'tommy','email':'tom@dl.com'}
        user = User(userDict['username'],userDict['pvid'],userDict['name'],userDict['email'])
        device = Device(devDict['address'],devDict['owner'],devDict['creator'],devDict['building'],devDict['type'],devDict['hostname'],devDict['IP'],devDict['nickname'])
        result = services.insert_and_sync_devices(user,device)
        print result
      #  self.assertEqual(result.IP,None)

class queryDB(unittest.TestCase):
    def test_queryTC1(self):
        print "Executing Testcase - API query_db_devices()"
        result = services.query_db_devices()
        print result

class uploadDev(unittest.TestCase):
    def test_uploadDevTC1(self):
        print "Executing Testcase - API upload_device()"
        devDict = {'address':'00-00-00-00-00-FF','IP':'172.16.14.34','owner':'Hello','creator':'sam','building':'ABW','type':'123','hostname':'sample','nickname':'tom'}
        device = Device(devDict['address'],devDict['owner'],devDict['creator'],devDict['building'],devDict['type'],devDict['hostname'],devDict['IP'],devDict['nickname'])
        result = services.upload_device(device)
        print result
def suite():

    testclass_torun = [readAddDev,insSyncDev,queryDB,uploadDev]
    loader = unittest.TestLoader()
    suites = []
    for test_class in testclass_torun:
        suite = loader.loadTestsFromTestCase(test_class)
        suites.append(suite)
    suite_torun = unittest.TestSuite(suites)
    return suite_torun

if __name__=='__main__':
    os.environ['MONGO_URI']="mongodb://mongodb:27017/deviceDB"
    runner=unittest.TextTestRunner()
    results=runner.run(suite())
