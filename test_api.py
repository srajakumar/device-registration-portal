#!/usr/bin/python


import api
from objects import *
import unittest
from selenium import webdriver
import time
from nose.tools import assert_true
import requests
import urllib3
import logging

class postClrPass(unittest.TestCase):
    def setUp(self):
        self.headers={'Accept':'application/json',
                'Autorization':'Bearer b2f3572899ae20c5ccb41f4fbac7e89b6cb08205',}
        self.URL= "https://172.16.76.134/api/device/mac/"
        urllib3.disable_warnings()
        logging.captureWarnings(True)
    def test_post_clrpassTC1(self):
        print "Executing Testcase - API post_clearpass()"
        MACdict = {'MAC':'00-00-00-00-00-00','owner':'Admin','creator':'Hello','buildingID':'ABW','typeID':'123','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'],MACdict['typeID'])
        result = api.post_clearpass(device)
        self.assertEqual(result,0)
        print result
    def test_post_clrpassTC2(self):
        print "Executing Testcase - API post_clearpass()"
        MACdict = {'MAC':'00:00:00:00:00:00','owner':'Admin1','creator':'Hello1','typeID':'123','buildingID':'ABW','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'],MACdict['typeID'])
        result = api.post_clearpass(device)
        self.assertNotEqual(result,0)
        print result
    def test_post_clrpassTC3(self):
        print "Executing Testcase - API post_clearpass()"
        MACdict = {'MAC':'00-00-00','owner':'Admin','creator':'Hello','buildingID':'ABT','typeID':'123','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'],MACdict['typeID'])
        result = api.post_clearpass(device)
        self.assertNotEqual(result,0)
        print result
    def test_post_clrpassTC4(self):
        print "Executing Testcase - API post_clearpass()"
        MACdict = {'MAC':'','owner':'Admin','creator':'Hello','buildingID':'ABM','typeID':'123','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'],MACdict['typeID'])
        result = api.post_clearpass(device)
        self.assertNotEqual(result,0)
        print result

class chkClrPass(unittest.TestCase):
    def setUp(self):
        self.headers={'Accept':'application/json',
                'Autorization':'Bearer b2f3572899ae20c5ccb41f4fbac7e89b6cb08205',}
        self.URL= "https://172.16.76.134/api/device/mac/"
        urllib3.disable_warnings()
        logging.captureWarnings(True)
    def test_chk_clrpassTC1(self):
        print "Executing Test case - API check_clearpass()" 
        MACdict = {'MAC':'00-00-00-00-00-00','owner':'Admin','creator':'Hello','buildingID':'ABW','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'])
        result = api.check_clearpass(device)
        self.assertEqual(result,True)
        print result
    def test_chk_clrpassTC2(self):
        print "Executing Test case - API check_clearpass()"
        MACdict = {'MAC':'00$00$00$00$00$00','owner':'Admin','creator':'Hello','buildingID':'ABW','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID']) 
        result = api.check_clearpass(device)
        self.assertEqual(result,True) 
        print result,"8888888888888888888888888888888888888888888888888888888888888"
    def test_chk_clrpassTC3(self):
        print "Executing Test case - API check_clearpass()"
        MACdict = {'MAC':'00-00-00','owner':'Admin','creator':'Hello','buildingID':'ABW','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'])
        result = api.check_clearpass(device)
        self.assertEqual(result,False)
        print result
    def test_chk_clrpassTC4(self):
        print "Executing Testcase - API check_clearpass()"
        MACdict = {'MAC':'','owner':'Admin','creator':'Hello','buildingID':'ABW','ownerID':'1','creatorID':'2'}
        device = Device(MACdict['MAC'],MACdict['owner'],MACdict['creator'],MACdict['ownerID'],MACdict['creatorID'],MACdict['buildingID'])
        result = api.check_clearpass(device)
        self.assertEqual(result,False)
        print result

class getClrPass(unittest.TestCase):
    def setUp(self):
        self.headers={'Accept':'application/json',
                'Autorization':'Bearer b2f3572899ae20c5ccb41f4fbac7e89b6cb08205',}
        self.URL= "https://172.16.76.134/api/device/mac/"
        urllib3.disable_warnings()
        logging.captureWarnings(True)
    def test_get_clrpassTC1(self):
        print "Executing Testcase - API get_clearpass()"
        result = api.get_clearpass('00-00-00-00-00-00')
        self.assertEqual(str(result.owner),"Admin")
        print result.owner
    def test_get_clrpassTC2(self):
        print "Executing Testcase - API get_clearpass()"
        result = api.get_clearpass('00-00-00')
        self.assertNotEqual(str(result.owner),"Admin")
        print result.owner
    def test_get_clrpassTC3(self):
        print "Executing Testcase - API get_clearpass()"
        result=api.get_clearpass('')
        self.assertNotEqual(str(result.owner),"Admin")
        print result.owner
    def test_get_clrpassTC4(self):
        print "Executing Testcase - API get_clearpass()"
        result = api.get_clearpass('00$00$00$00$00$00')
        self.assertEqual(str(result.owner),"Admin")
        print result.owner


class delClrPass(unittest.TestCase):
    def setUp(self):
        self.headers={'Accept':'application/json',
                'Autorization':'Bearer b2f3572899ae20c5ccb41f4fbac7e89b6cb08205',}
        self.URL= "https://172.16.76.134/api/device/mac/"
        urllib3.disable_warnings()
        logging.captureWarnings(True)
    def test_delete_clrpassTC1(self):
        print "Executing Testcase - API delete_clearpass()"
        result = api.delete_clearpass('00-00-00-00-00-00')
        self.assertEqual(result,0)
        print result
    def test_delete_clrpassTC2(self):
        print "Executing Testcase - API delete_clearpass()"
        result = api.delete_clearpass('00-00-00')
        self.assertNotEqual(result,0)
        print result
    def test_delete_clrpassTC3(self):
        print "Executing Testcase - API delete_clearpass()"
        result=api.delete_clearpass('')
        self.assertNotEqual(result,0)
        print result
    def test_delete_clrpassTC4(self):
        print "Executing Testcase - API delete_clearpass()"
        result = api.delete_clearpass('00:00:00:00:00:00')
        self.assertNotEqual(result,0)
        print result
 
    def tearDown(self):
        pass


def suite():

    testclass_torun = [postClrPass,chkClrPass,getClrPass,delClrPass]
    loader = unittest.TestLoader()
    suites = []
    for test_class in testclass_torun:
        suite = loader.loadTestsFromTestCase(test_class)
        suites.append(suite)
    suite_torun = unittest.TestSuite(suites)
    return suite_torun

if __name__=='__main__':
   runner =unittest.TextTestRunner()
   results=runner.run(suite()) 
